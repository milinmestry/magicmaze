#Task - command line game:

Create the following game with a CLI interface in PHP. For us the overall class design is important.

You have a player that has a name and moves around in a maze. The maze consists of rooms with exits. The rooms are in a 2D environment. Each room can have multiple exits. The player can go from one room to another.

Each room can have certain items in it (e.g., a sword, a bomb, a banana etc.). The player has a backpack that can contain a certain amount of items, depending on the item weight. So the player can pick up an item in each room, but he can also drop one to pick up another.

Please implement a command-line solution of the game that just prints out text, depending on the commands:

- help = prints the list of commands
- location = prints out the current player location and the list of exits
- go to “room name” = player will move to the exit that connects the current room with the “room name” and will execute the location command
- items = prints out the list of items in the backpack
- pick up “item name” = will pickup the item and execute the items command
- drop “item name” = will drop the item and execute the items command
- quit = exits the game

You don’t have to implement the following features, but you might take them into account:

- player has a health status and the more he walks around the more he loses
- some rooms contain monsters and player may fight them
- multiple players
- some exits are impossible to pass unless the player has a key
- some exits have to be broken given that the player has a specific item

## Usage

`php -f index.php`

```shell

milin@Microland-Game-web:/var/www/html$ php -f index.php

Command: help
-------------------------------
Welcome to Microland Maze Game.
-------------------------------
Use below commands:
___________________
Move player to new room-> php -f index.php "go to" "room name"
Get player current location-> php -f index.php "location"
To pick up an item-> php -f index.php "pick up" "Banana"
To drop an item-> php -f index.php "drop" "Bomb"
To end the game-> php -f index.php "quit"
-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x
```

|Description|Syntax|Example                                                                    |
|-----------|------|---------------------------------------------------------------------------|
|Move player to new room|php -f index.php "go to" "room name"|php -f index.php "go to" "room#1"|
|To pick up an item|php -f index.php "pick up" "Banana"|php -f index.php "pick up" "Banana"|
|To end the game|php -f index.php "quit"|php -f index.php "quit"|

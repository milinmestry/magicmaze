<?php
namespace Microland\Mazegame\Interfaces;

interface Name {
  public function setName(string $name);
  public function getName() : string;
}
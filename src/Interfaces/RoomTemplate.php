<?php
namespace Microland\Mazegame\Interfaces;

interface RoomTemplate {
  public function setCanExitFromIt(bool $answer);
  public function setConnectedRooms(array $rooms);
  public function setNumberOfDoors(int $doors);
  public function setRoomName(string $name);
  public function getRoom();
}
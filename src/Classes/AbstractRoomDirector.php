<?php
namespace Microland\Mazegame\Classes;

abstract class AbstractRoomDirector {

  abstract public function buildRoom();
  abstract public function getRoom();
}
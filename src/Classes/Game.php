<?php
namespace Microland\Mazegame\Classes;

use Microland\Mazegame\Classes\Item;
use Microland\Mazegame\Classes\Maze;
use Microland\Mazegame\Classes\Player;
use Microland\Mazegame\Classes\ItemRoom\RoomItemDirector;
use Microland\Mazegame\Classes\ItemRoom\RoomWithItemBuilder;
use Microland\Mazegame\Classes\MonsterRoom\RoomMonsterDirector;
use Microland\Mazegame\Classes\MonsterRoom\RoomWithMonsterBuilder;
use Microland\Mazegame\Classes\SpecialKeyRoom\RoomSpecialKeyDirector;
use Microland\Mazegame\Classes\SpecialKeyRoom\RoomWithSpecialKeyBuilder;
use Exception;
use Throwable;

class Game {
  private $player;
  private $maze;

  public function __construct(string $playerName) {
    $this->init($playerName);
    $this->addRoomsToMaze();
  }

  public function init(string $playerName) {
    $this->maze = new Maze();
    $this->player = new Player($playerName);
    // $this->player->setName($playerName);
  }

  protected function getRoomAttr(
    int $roomNum,
    array $connectedRooms = [],
    bool $canExit = false,
    int $numOfDoors = 1,
  ) : array {
    return [
      CAN_EXIT => $canExit,
      NUMBER_OF_DOORS => $numOfDoors,
      CONNECTED_ROOMS => $connectedRooms,
      ROOM_NAME => ROOM . '#' . $roomNum,
    ];
  }

  public function createItemRoom(
    string $itemName,
    int $roomNum,
    array $connectedRooms = [],
    bool $canExit = false,
    int $numOfDoors = 1,
  ) : void {
    $attr = $this->getRoomAttr(
      $roomNum,
      $connectedRooms,
      $canExit,
      $numOfDoors,
    );
    $attr[ITEM] = $itemName;

    $builder = new RoomWithItemBuilder();
    $director = new RoomItemDirector();
    $director->setBuilder($builder);
    $director->buildRoom($attr);
    $this->maze->addRoom($director->getRoom());
  }

  public function createMonsterRoom(
    int $roomNum,
    array $connectedRooms = [],
    bool $canExit = false,
    int $numOfDoors = 1,
  ) : void {
    $builder = new RoomWithMonsterBuilder();
    $director = new RoomMonsterDirector();
    $director->setBuilder($builder);
    $director->buildRoom($this->getRoomAttr(
      $roomNum,
      $connectedRooms,
      $canExit,
      $numOfDoors,
    ));
    $this->maze->addRoom($director->getRoom());
  }

  public function createSpecialKeyRoom(
    int $roomNum,
    array $connectedRooms = [],
    bool $canExit = false,
    int $numOfDoors = 1,
  ) : void {
    $builder = new RoomWithSpecialKeyBuilder();
    $director = new RoomSpecialKeyDirector();
    $director->setBuilder($builder);
    $director->buildRoom($this->getRoomAttr(
      $roomNum,
      $connectedRooms,
      $canExit,
      $numOfDoors,
    ));
    $this->maze->addRoom($director->getRoom());
  }

  public function addRoomsToMaze() : int {
    $this->createItemRoom(BANANA, 1, []);
    $this->createMonsterRoom(2, []);
    $this->createSpecialKeyRoom(3, [], true);
    return $this->maze->getTotalRooms();
  }

  public function movePlayerTo(string $roomName) : string {
    try {
      $room = $this->maze->checkRoomExists($roomName);
      if (!$room) {
        throw new Exception('Room is not created.', 1);
      }
      return $this->player->movePlayerTo($room)->getDetails();
    } catch (Throwable $th) {
      throw $th;
    }
  }

  public function pickItem(string $itemName) : string {
    try {
      $item = new Item($itemName);
      $item->printItems();
      return $this->player->pickItem($item)->getDetails();
    } catch (Throwable $th) {
      throw $th;
    }
  }

}

<?php
namespace Microland\Mazegame\Classes\SpecialKeyRoom;

use Microland\Mazegame\Classes\SpecialKeyRoom\RoomWithSpecialKey;
use Microland\Mazegame\Interfaces\RoomTemplate;


class RoomWithSpecialKeyBuilder implements RoomTemplate {

  private $room;

  public function __construct() {
    $this->reset();
  }

  public function reset(): void {
    $this->room = new RoomWithSpecialKey();
  }

  public function setSpecialKey() {
    $this->room->setSpecialKey();
  }

  public function setCanExitFromIt(bool $answer) : object {
    $this->room->setCanExitFromIt($answer);
    return $this;
  }

  public function setConnectedRooms(array $rooms = []) : object {
    $this->room->setConnectedRooms($rooms);
    return $this;
  }

  public function setNumberOfDoors(int $doors) : object {
    $this->room->setNumberOfDoors($doors);
    return $this;
  }

  public function setRoomName(string $name) : object {
    $this->room->setRoomName($name);
    return $this;
  }

  public function getRoom() : RoomWithSpecialKey {
    return $this->room;
    // $this->reset();
    // return $room;
  }


}
<?php
namespace Microland\Mazegame\Classes\SpecialKeyRoom;

use Microland\Mazegame\Classes\Room;

class RoomWithSpecialKey extends Room {

  protected $specialKey;

  public function __construct() {
    parent::__construct();
  }

  public function setSpecialKey() {
    $this->specialKey = true;
  }

  public function hasSpecialKey() : bool {
    return $this->specialKey ?? false;
  }

  public function getDistance() : int {
    return 4;
  }

  public function printOut() {

  }


}
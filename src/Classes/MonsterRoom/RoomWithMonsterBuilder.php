<?php
namespace Microland\Mazegame\Classes\MonsterRoom;

use Microland\Mazegame\Classes\MonsterRoom\RoomWithMonster;
use Microland\Mazegame\Interfaces\RoomTemplate;


class RoomWithMonsterBuilder implements RoomTemplate {

  private $room;

  public function __construct() {
    $this->reset();
  }

  public function reset(): void {
    $this->room = new RoomWithMonster();
  }

  public function setMonster(string $name) {
    $this->room->setMonster($name);
  }

  public function setCanExitFromIt(bool $answer) : object {
    $this->room->setCanExitFromIt($answer);
    return $this;
  }

  public function setConnectedRooms(array $rooms) : object {
    $this->room->setConnectedRooms($rooms);
    return $this;
  }

  public function setNumberOfDoors(int $doors) : object {
    $this->room->setNumberOfDoors($doors);
    return $this;
  }

  public function setRoomName(string $name) : object {
    $this->room->setRoomName($name);
    return $this;
  }

  public function getRoom() : RoomWithMonster {
    return $this->room;
    // $this->reset();
    // return $room;
  }


}
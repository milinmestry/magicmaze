<?php
namespace Microland\Mazegame\Classes\MonsterRoom;

use Microland\Mazegame\Classes\Room;

class RoomWithMonster extends Room {

  protected $monster;

  public function __construct() {}

  public function setMonster(string $name) {
    $this->monster = $name;
  }

  public function getDistance() : int {
    return 3;
  }

  public function printOut() {

  }


}
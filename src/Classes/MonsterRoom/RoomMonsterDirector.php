<?php
namespace Microland\Mazegame\Classes\MonsterRoom;

use Microland\Mazegame\Classes\AbstractRoomDirector;
use Microland\Mazegame\Classes\MonsterRoom\RoomWithMonsterBuilder;

class RoomMonsterDirector extends AbstractRoomDirector {

  private $builder;

  public function setBuilder(RoomWithMonsterBuilder $builder) {
    $this->builder = $builder;
  }

  public function buildRoom(array $params = []) {
    $this->builder
      ->setCanExitFromIt($params[CAN_EXIT] ?? false)
      ->setNumberOfDoors($params[NUMBER_OF_DOORS] ?? 0)
      ->setConnectedRooms($params[CONNECTED_ROOMS] ?? null)
      ->setRoomName($params[ROOM_NAME] ?? null);
  }

  public function getRoom() {
    return $this->builder->getRoom();
  }
}

<?php
namespace Microland\Mazegame\Classes;

use Microland\Mazegame\Interfaces\Name;

class NameHolder implements Name {

  private $name;

  public function setName(string $name = '') : object {
    $this->name = $name;
    return $this;
  }

  public function getName() : string {
    return $this->name;
  }
}
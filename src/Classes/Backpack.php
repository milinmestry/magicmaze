<?php
namespace Microland\Mazegame\Classes;

use Exception;

class Backpack {

  const MAX_CAPACITY = 100;

  private $capacity;
  private $storage;

  public function __construct() {
    $this->setCapacity(self::MAX_CAPACITY)
      ->resetStorage();
  }

  public function setCapacity(int $capacity) : object {
    $this->capacity = $capacity;
    return $this;
  }

  public function getCapacity() : int {
    return $this->capacity;
  }

  public function addToCapacity(Item $item) : object {
    $total = $this->getCapacity();
    $total += $this->totalItemsWeight($item);
    return $this->setCapacity($total);
  }

  public function removeFromCapacity(Item $item) : object {
    $total = $this->getCapacity();
    $total -= $this->totalItemsWeight($item);
    return $this->setCapacity($total);
  }

  public function resetStorage() : object {
    $this->storage = [];
    return $this;
  }

  public function addItem(Item $item) : object {
    if ($this->getCapacity() > self::MAX_CAPACITY) {
      throw new Exception('Backpack capacity full.', 1);
    }

    if ($this->itemExist($item)) {
      $this->storage[$item->getName()] += 1;
    } else {
      $this->storage[$item->getName()] = 1;
    }

    $this->removeFromCapacity($item);
    return $this;
  }

  public function removeItem(Item $item) : object {
    if ($this->itemExist($item)) {
      $this->storage[$item->getName()] -= 1;
      $this->addToCapacity($item);
      // if all the items of a type are removed, delete from storage
      $this->checkAndRemoveItem($item);
    }
    return $this;
  }

  public function itemExist(Item $item) : bool {
    return $this->storage[$item->getName()] ?? false;
  }

  public function countAllTotalItems() : int {
    return array_sum($this->storage);
    // return count($this->storage);
  }

  public function totalItemsByName(Item $item) : int {
    return $this->storage[$item->getName()];
  }

  public function totalItemsWeight(Item $item) : int {
    return $item->getItemWeight($item->getName()) * $this->totalItemsByName($item);
  }

  public function checkAndRemoveItem(Item $item) : void {
    if ($this->totalItemsByName($item) <= 0) {
      unset($this->storage[$item->getName()]);
    }
  }

  public function getDetails() : string {
    return 'Backpack has total items [' . $this->countAllTotalItems()
      . '] and remaining backpack capacity [' . $this->getCapacity()
      . '].' . PHP_EOL;
  }
}
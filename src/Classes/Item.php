<?php
namespace Microland\Mazegame\Classes;

use Exception;

class Item extends NameHolder {

  private $weight;

  public function __construct(string $name = '') {
    $this->init($name);
  }

  protected function init(string $name = '') {
    if (empty($name)) {
      throw new Exception('Item name is empty', 1);
    }
    if (!$this->checkItemExists($name)) {
      throw new Exception('Item not in allowed list.', 1);
    }
    // $this->printItems($this->itemsWithWeight());

    $this->setName($name)->setWeight($this->getItemWeight($name));
  }

  public function checkItemExists(string $name) : bool {
    return array_key_exists($name, $this->itemsWithWeight());
  }

  public function setWeight(int $weight) : object {
    $this->weight = $weight;
    return $this;
  }

  public function getWeight() : int {
    return $this->weight;
  }

  public function itemsWithWeight() : array {
    return [
      BANANA => 4,
      BOMB => 10,
      HAMMER => 3,
      SWORD => 15,
      SHIELD => 18,
    ];
  }

  public function printItems() : void {
    print_r($this->itemsWithWeight());
  }

  public function getItemWeight(string $item = '') : int {
    return $this->itemsWithWeight()[$item];
  }

}
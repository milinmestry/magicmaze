<?php
namespace Microland\Mazegame\Classes;

use Exception;
use Microland\Mazegame\Classes\Room;

class Maze {
  private $rooms;


  public function __constuct() {
    $this->rooms = [];
  }

  public function addRoom(Room $room) {
    $oldRoom = $this->checkRoomExists($room->getRoomName());
    if ($oldRoom) {
      throw new Exception($oldRoom->getRoomName() . ' room already created.', 1);
    }
    $this->rooms[$room->getRoomName()] = $room;
  }

  public function checkRoomExists(string $roomName) {
    return $this->rooms[$roomName] ?? false;
  }

  public function getTotalRooms() : int {
    return count($this->rooms ?? []);
  }
}
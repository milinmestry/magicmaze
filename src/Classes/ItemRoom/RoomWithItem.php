<?php
namespace Microland\Mazegame\Classes\ItemRoom;

use Microland\Mazegame\Classes\Item;
use Microland\Mazegame\Classes\Room;

class RoomWithItem extends Room {

  protected $item;

  public function __construct() {
    parent::__construct();
  }

  public function setItem(string $name) {
    $this->item = new Item($name);
  }

  public function getDistance() : int {
    return 2;
  }

  public function printOut() {

  }


}
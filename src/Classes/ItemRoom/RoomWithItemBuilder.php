<?php
namespace Microland\Mazegame\Classes\ItemRoom;

use Microland\Mazegame\Classes\ItemRoom\RoomWithItem;
use Microland\Mazegame\Interfaces\RoomTemplate;

class RoomWithItemBuilder implements RoomTemplate {

  private $room;

  public function __construct() {
    $this->reset();
  }

  public function reset(): void {
    $this->room = new RoomWithItem();
  }

  public function setItem(string $name) : object {
    $this->room->setItem($name);
    return $this;
  }

  public function setCanExitFromIt(bool $answer) : object {
    $this->room->setCanExitFromIt($answer);
    return $this;
  }

  public function setConnectedRooms(array $rooms) : object {
    $this->room->setConnectedRooms($rooms);
    return $this;
  }

  public function setNumberOfDoors(int $doors) : object {
    $this->room->setNumberOfDoors($doors);
    return $this;
  }

  public function setRoomName(string $name) : object {
    $this->room->setRoomName($name);
    return $this;
  }

  public function getRoom() : RoomWithItem {
    return $this->room;
    // $this->reset();
    // return $room;
  }


}
<?php
namespace Microland\Mazegame\Classes\ItemRoom;

use Microland\Mazegame\Classes\AbstractRoomDirector;
use Microland\Mazegame\Classes\ItemRoom\RoomWithItemBuilder;

class RoomItemDirector extends AbstractRoomDirector {

  private $builder;

  public function setBuilder(RoomWithItemBuilder $builder) {
    $this->builder = $builder;
  }

  public function buildRoom(array $params = []) {
    $this->builder
      ->setItem($params[ITEM])
      ->setCanExitFromIt($params[CAN_EXIT] ?? false)
      ->setNumberOfDoors($params[NUMBER_OF_DOORS] ?? 0)
      ->setConnectedRooms($params[CONNECTED_ROOMS] ?? null)
      ->setRoomName($params[ROOM_NAME] ?? null);
  }

  public function getRoom() {
    return $this->builder->getRoom();
  }
}

<?php
namespace Microland\Mazegame\Classes;

use Microland\Mazegame\Classes\Backpack;
use Microland\Mazegame\Classes\ItemRoom\RoomWithItem;
use Microland\Mazegame\Classes\NameHolder;
use Microland\Mazegame\Classes\Room;
use Exception;

class Player extends NameHolder {

  const PLAYER_MAX_HEALTH = 100;

  private $backpack;
  private $currentRoom;
  private $distanceCovered;
  private $health;
  // private $items;


  public function __construct(string $name = '') {
    $this->init($name);
  }

  protected function init(string $name = '') {
    if (empty($name)) {
      throw new Exception('Player name is empty', 1);
    }
    $this->setName($name)
      ->assignBackpack(new Backpack())
      // ->resetItems()
      ->setHealth(self::PLAYER_MAX_HEALTH);
  }

  // public function resetItems() : object {
  //   $this->items = [];
  //   return $this;
  // }

  public function setHealth(int $health) : object {
    $this->health = $health;
    return $this;
  }

  public function getHealth() : int {
    return $this->health;
  }

  public function assignBackpack(Backpack $backpack) : object {
    $this->backpack = $backpack;
    return $this;
  }

  public function pickItem(Item $item) : object {
    $this->backpack->addItem($item);
    // $this->items[] = $item;
    return $this->backpack;
  }

  public function dropItem(Item $item) : object {
    $this->backpack->removeItem($item);
    return $this;
  }

  public function movePlayerTo(Room $room) : object {
    $this->setCurrentRoom($room);
    // Update distabced covered by player
    $this->addDistancedCovered($room->getDistance());
    // If player moves to another room, he will loose some health
    $this->setHealth($this->getHealth() - $room->getDistance());
    return $this;
  }

  public function setCurrentRoom(Room $room) : object {
    $this->currentRoom = $room;
    return $this;
  }

  public function getCurrentRoom() : RoomWithItem {
    return $this->currentRoom;
  }

  public function addDistancedCovered(int $distance) : object {
    $this->distanceCovered += $distance;
    return $this;
  }

  public function getDistancedCovered() : int {
    return $this->distanceCovered;
  }

  public function getDetails() : string {
    return PLAYER . ' [' . $this->getName() . '] is in the room ['
      . $this->currentRoom->getRoomName()
      . '], and has distance covered ['
      . $this->getDistancedCovered() . ']. '
      . PLAYER . ' health is [' . $this->getHealth() . '].' . PHP_EOL
      . $this->backpack->getDetails();
  }

}
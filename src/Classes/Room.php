<?php
namespace Microland\Mazegame\Classes;


/**
 *
 */
class Room {

  private $canExit;
  private $numberOfConnectedRooms;
  private $numberOfDoors;
  private $roomName;

  public function __construct() {
  }

  public function setCanExitFromIt(bool $answer) : object {
    $this->canExit = $answer;
    return $this;
  }

  public function setConnectedRooms(array $rooms) : object {
    $this->numberOfConnectedRooms = $rooms;
    return $this;
  }

  public function setNumberOfDoors(int $doors) : object {
    $this->numberOfDoors = $doors;
    return $this;
  }

  public function setRoomName(string $name) : object {
    $this->roomName = $name;
    return $this;
  }

  public function getRoomName() : string {
    return $this->roomName;
  }
}

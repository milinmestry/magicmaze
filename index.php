<?php
declare(strict_types = 1);
require __DIR__ . '/bootstrap.php';

use Microland\Mazegame\Classes\Game;


try {
  // var_dump($argv);
  $command = $argv[1] ?? HELP;
  $argv2 = $argv[2] ?? null;
  // echo $command . PHP_EOL;

  $game = new Game('Matt');

  echo COMMAND , ': ', $command , ' ', $argv2, PHP_EOL;

  switch ($command) {
    case HELP:
      echo '-------------------------------' , PHP_EOL;
      echo 'Welcome to Microland Maze Game.' , PHP_EOL;
      echo '-------------------------------' , PHP_EOL;
      echo 'Use below commands:' , PHP_EOL;
      echo '___________________' , PHP_EOL;
      echo 'Move player to new room-> php -f index.php "', GO_TO, '" "room name"' , PHP_EOL;
      echo 'Get player current location-> php -f index.php "', LOCATION, '"' , PHP_EOL;
      echo 'To pick up an item-> php -f index.php "', PICK_UP, '" "Banana"' , PHP_EOL;
      echo 'To drop an item-> php -f index.php "', DROP, '" "Bomb"' , PHP_EOL;
      echo 'To end the game-> php -f index.php "', QUIT, '"' , PHP_EOL;
      break;
    case DROP:
      echo 'DROP_ITEM' . PHP_EOL;
      break;
    case GO_TO:
      echo $game->movePlayerTo($argv2);
      break;
    case LOCATION:
      echo 'player location' . PHP_EOL;
      break;
    case PICK_UP:
        // echo 'PICK_ITEM' . PHP_EOL;
        echo $game->pickItem($argv2);
        break;
    case QUIT:
      echo 'Thank you for playing the game.' . PHP_EOL;
      break;
    default:
      echo 'Unknown game command.' . PHP_EOL;
  }
} catch (\Throwable $th) {
  echo $th->getMessage() , PHP_EOL;
  echo $th->getTraceAsString() , PHP_EOL;
}
echo '-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x' , PHP_EOL;
